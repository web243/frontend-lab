const express = require('express');
const morgan = require('morgan');
const filesRouter = require('./routers/files_router/files_router');

const app = express();
const port = 8080;

app.use(morgan('tiny'));
app.use(express.json());

app.use('/api/files', filesRouter);
app.listen(port, () => {
    console.log(`Server is listening at port ${port}`);
});

// @TODO: check if passwords work
