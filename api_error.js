const DEFAULT_MESSAGES = require('./default_response_messages');

class APIError extends Error {
    constructor(status, message = DEFAULT_MESSAGES[status]) {
        super(message);
        this.status = status;
    }
}

module.exports = APIError;
