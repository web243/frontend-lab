const DEFAULT_MESSAGES = {
    200: 'Success',
    400: 'Bad request',
    500: 'Server error',
};

module.exports = DEFAULT_MESSAGES;
