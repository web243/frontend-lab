const express = require('express');
const fsPromises = require('fs').promises;
const {
    formResponseForFileProcessingError,
} = require('./files_processing/response_data_formatter');
const {
    validate,
    validateContent,
} = require('./files_processing/files_data_validation');
const { createPasswordIfSpecified } = require('./files_processing/passwords/files_passwords_keeper');
const {
    getAccessToDirectoryForFile,
    getAccessToFile,
    getAccessToDirectory,
} = require('./files_processing/files_access');
const DEFAULT_MESSAGES = require('../../default_response_messages');
const { FILES_DIR } = require('./files_processing/paths/files_dirs');
const { getFileData } = require('./files_processing/file_reader');
const { writeFile, deleteFile } = require('./files_processing/file_writer');

const filesRouter = express.Router();

filesRouter.get('/:filename', (req, res) => {
    const { filename } = req.params;
    const { password } = req.query;
    getAccessToFile(filename, password)
        .then(() => getFileData(filename, password))
        .then((data) => {
            const body = { ...data };
            body.message = 'Success';
            res.status(200).json(body);
        })
        .catch((e) => {
            const { status, message } = formResponseForFileProcessingError(e);
            res.status(status).json({ message });
        });
});

filesRouter.get('/', (req, res) => {
    (async () => {
        try {
            await getAccessToDirectory(FILES_DIR);
            const files = await fsPromises.readdir(FILES_DIR);
            const code = 200;
            res.status(code).json({ files, message: DEFAULT_MESSAGES[code] });
        } catch (e) {
            const code = 500;
            res.status(code).json({ message: DEFAULT_MESSAGES[code] });
        }
    })();
});

filesRouter.post('/', (req, res) => {
    const params = {
        filename: req.body.filename,
        content: req.body.content,
        password: req.body.password,
    };
    validate(params)
        .then(() => getAccessToDirectoryForFile(params.filename, params.password))
        .then(() => createPasswordIfSpecified(params.filename, params.password))
        .then(() => writeFile(params))
        .then(() => {
            const message = 'File created successfully';
            res.status(200).json({ message });
        })
        .catch((e) => {
            const { status, message } = formResponseForFileProcessingError(e);
            res.status(status).json({ message });
        });
});

filesRouter.patch('/:filename', (req, res) => {
    const { filename } = req.params;
    const newContent = req.body.content;
    const { password } = req.query;
    getAccessToFile(filename, password)
        .then(() => validateContent(newContent))
        .then(() => writeFile({ filename, password, content: newContent }))
        .then(() => {
            const message = 'File content updated successfully';
            res.status(200).json({ message });
        })
        .catch((e) => {
            const { status, message } = formResponseForFileProcessingError(e);
            res.status(status).json({ message });
        });
});

filesRouter.delete('/:filename', (req, res) => {
    const { filename } = req.params;
    const { password } = req.query;
    getAccessToFile(filename, password)
        .then(() => deleteFile({ filename, password }))
        .then(() => {
            const message = 'File deleted successfully';
            res.status(200).json(message);
        })
        .catch((e) => {
            const { status, message } = formResponseForFileProcessingError(e);
            res.status(status).json({ message });
        });
});

module.exports = filesRouter;
