const path = require('path');
const { FILES_DIR, PROTECTED_FILES_DIR } = require('./files_dirs');
const { considerPasswordSpecified } = require('../passwords/password_preliminary_check');

function getDirPathForFile(filename, password) {
    let fileDir = FILES_DIR;
    const hasPassword = considerPasswordSpecified(password);
    if (hasPassword) {
        fileDir = PROTECTED_FILES_DIR;
    }
    return fileDir;
}

function getFilepath(filename, password) {
    const fileDir = getDirPathForFile(filename, password);
    return path.join(fileDir, filename);
}

module.exports = {
    getFilepath,
    getDirPathForFile,
};
