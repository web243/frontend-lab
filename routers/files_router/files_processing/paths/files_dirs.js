const path = require('path');

const MAIN_DIR = 'data';
const FILES_DIR = path.join(MAIN_DIR, 'main');
const PROTECTED_FILES_DIR = path.join(MAIN_DIR, 'protected');

module.exports = {
    MAIN_DIR,
    FILES_DIR,
    PROTECTED_FILES_DIR,
};
