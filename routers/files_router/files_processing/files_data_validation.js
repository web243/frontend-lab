const fsPromises = require('fs').promises;
const { getPasswordRecord } = require('./passwords/files_passwords_keeper');
const APIError = require('../../../api_error');
const { getExtension } = require('./filename_parser');
const { getFilepath } = require('./paths/path_formatter');

const SUPPORTED_EXTENSIONS = ['log', 'txt', 'json', 'yaml', 'xml', 'js'];
const MANDATORY_FIELDS = ['filename', 'content'];

function isExtensionSupported(filename) {
    const fileExtension = getExtension(filename);
    return !!SUPPORTED_EXTENSIONS.find((extension) => extension === fileExtension);
}

function isFieldMandatory(field) {
    return !!MANDATORY_FIELDS.find((mandatoryField) => mandatoryField === field);
}

function checkIfPasswordMatches(filename, passwordToCheck) {
    if (passwordToCheck.length === 0) {
        return Promise.reject(new APIError(400, 'Password is empty'));
    }
    return getPasswordRecord(filename)
        .then((record) => {
            const password = record[1];
            if (password === passwordToCheck) {
                return Promise.resolve();
            }
            return Promise.reject(new APIError(400, 'Invalid password'));
        });
}

function checkIfFileExists(filepath) {
    return fsPromises.access(filepath)
        .then(() => Promise.resolve(true))
        .catch(() => Promise.resolve(false));
}

function checkIfAllParamsSpecified(filedata) {
    // eslint-disable-next-line no-restricted-syntax
    for (const [field, value] of Object.entries(filedata)) {
        if (isFieldMandatory(field) && !value) {
            const message = `Please specify '${field}' parameter`;
            return Promise.reject(new APIError(400, message));
        }
    }
    return Promise.resolve();
}

function checkIfExtensionIsSupported(filename) {
    const extensionSupported = isExtensionSupported(filename);
    if (!extensionSupported) {
        const fileExtension = getExtension(filename);
        const message = `Extension '${fileExtension}' is not supported`;
        return Promise.reject(new APIError(400, message));
    }
    return Promise.resolve();
}

function validateContent(content) {
    if (!content) {
        return Promise.reject(new APIError(400, 'Please specify content'));
    }
    return Promise.resolve();
}

function validate(filedata) {
    return checkIfAllParamsSpecified(filedata)
        .then(() => checkIfExtensionIsSupported(filedata.filename))
        .then(() => {
            const filepath = getFilepath(filedata.filename, filedata.password);
            return checkIfFileExists(filepath);
        });
}

module.exports = {
    checkIfPasswordMatches,
    validateContent,
    validate,
    checkIfFileExists
};
