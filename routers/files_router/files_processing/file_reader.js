const fsPromises = require('fs').promises;
const { getExtension } = require('./filename_parser');
const { getFilepath } = require('./paths/path_formatter');

function getFileData(filename, password) {
    const filepath = getFilepath(filename, password);
    const data = {
        filename,
        extension: getExtension(filename),
    };
    return fsPromises.readFile(filepath, 'utf8')
        .then((content) => {
            data.content = content;
            return fsPromises.stat(filepath);
        })
        .then((stat) => {
            data.uploadedDate = stat.birthtime;
            return data;
        });
}

module.exports = {
    getFileData,
};
