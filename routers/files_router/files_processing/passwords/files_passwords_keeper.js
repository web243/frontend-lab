const fsPromises = require('fs').promises;
const Papa = require('papaparse');
const APIError = require('../../../../api_error');
const { considerPasswordSpecified } = require('./password_preliminary_check');

const PASSWORDS_FILE = 'passwords.csv';

function getFilenameFromRecord(record) {
    return record[0];
}

function formCsvRecord(filename, password) {
    const csv = Papa.unparse([[filename, password]]);
    return `\n${csv}`;
}

function getRecords() {
    return fsPromises.readFile(PASSWORDS_FILE, 'utf8')
        .then((data) => {
            return Papa.parse(data, { newline: '\n' }).data;
        });
}

function formCsvString(records) {
    return Papa.unparse(records);
}

function deleteFileRecord(records, filename) {
    for (let i = 0; i < records.length; i += 1) {
        const record = records[i];
        const filenameFromRecord = getFilenameFromRecord(record);
        if (filenameFromRecord === filename) {
            records.splice(i, 1);
            break;
        }
    }
}

function writeRecords(records) {
    const csv = formCsvString(records);
    return fsPromises.writeFile(PASSWORDS_FILE, csv);
}

function getPasswordRecord(filename) {
    return getRecords()
        .then((records) => {
            // eslint-disable-next-line no-restricted-syntax
            for (const record of records) {
                const filenameFromRecord = getFilenameFromRecord(record);
                if (filenameFromRecord === filename) {
                    return Promise.resolve(record);
                }
            }
            return Promise.reject(new APIError(500));
        });
}

function addNewPasswordRecord(filename, password) {
    const record = formCsvRecord(filename, password);
    return fsPromises.appendFile(PASSWORDS_FILE, record);
}

function createPassword(filename, password) {
    if (!password) {
        return Promise.reject(new APIError(400, 'Password is not specified'));
    }
    return addNewPasswordRecord(filename, password);
}

function createPasswordIfSpecified(filename, password) {
    const hasPassword = considerPasswordSpecified(password);
    if (hasPassword) {
        return createPassword(filename, password);
    }
    return Promise.resolve();
}

function deletePasswordRecordIfNeeded(filename) {
    return getRecords()
        .then((records) => {
            deleteFileRecord(records, filename);
            return writeRecords(records);
        });
}

module.exports = {
    getPasswordRecord,
    createPasswordIfSpecified,
    deletePasswordRecordIfNeeded,
};
