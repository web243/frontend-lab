function considerPasswordSpecified(password) {
    return !!password || typeof password === 'string';
}

module.exports = {
    considerPasswordSpecified,
};
