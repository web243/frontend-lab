const DEFAULT_MESSAGES = require('../../../default_response_messages');

function getErrorStatus(error) {
    return error.status ? error.status : 500;
}

function getErrorMessage(error) {
    return error.status === 500 ? DEFAULT_MESSAGES['500'] : error.message;
}

function formResponseForFileProcessingError(error, filename) {
    let status;
    let message;
    const notFound = error.code === 'ENOENT';
    if (notFound) {
        message = `No file with '${filename}' filename found`;
        status = 400;
    } else {
        status = getErrorStatus(error);
        message = getErrorMessage(error);
    }
    return { status, message };
}

module.exports = {
    formResponseForFileProcessingError,
};
