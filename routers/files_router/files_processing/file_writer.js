const fsPromises = require('fs').promises;
const { getFilepath } = require('./paths/path_formatter');
const {deletePasswordRecordIfNeeded} = require('./passwords/files_passwords_keeper');

function writeFile(params) {
    const { filename, password, content } = params;
    const filepath = getFilepath(filename, password);
    return fsPromises.writeFile(filepath, content);
}

function deleteFile(params) {
    const {filename, password} = params;
    const filepath = getFilepath(filename, password);
    return fsPromises.unlink(filepath)
        .then(() => deletePasswordRecordIfNeeded(filename));
}

module.exports = {
    writeFile,
    deleteFile
};
