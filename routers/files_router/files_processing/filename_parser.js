const path = require('path');

function getExtension(filename) {
    return path.extname(filename).substring(1);
}

module.exports = {
    getExtension,
};
