const fsPromises = require('fs').promises;
const { getFilepath, getDirPathForFile } = require('./paths/path_formatter');
const { MAIN_DIR } = require('./paths/files_dirs');
const { checkIfPasswordMatches, checkIfFileExists } = require('./files_data_validation');
const { considerPasswordSpecified } = require('./passwords/password_preliminary_check');
const APIError = require('../../../api_error');

function getAccessToMainDirectory() {
    return fsPromises.access(MAIN_DIR)
        .catch(() => fsPromises.mkdir(MAIN_DIR));
}

function getAccessToDirectory(dirpath) {
    return getAccessToMainDirectory()
        .then(() => fsPromises.access(dirpath))
        .catch(() => fsPromises.mkdir(dirpath));
}

function getAccessToDirectoryForFile(filename, password) {
    const dirPath = getDirPathForFile(filename, password);
    return getAccessToDirectory(dirPath);
}

function getAccessToFile(filename, passwordReceived) {
    const filepath = getFilepath(filename, passwordReceived);
    return getAccessToDirectoryForFile(filename, passwordReceived)
        .then(() => checkIfFileExists(filepath))
        .then((exists) => {
            if (!exists) {
                const message = `No file with '${filename}' name found. `
                    + 'If the file is protected, consider specifying password';
                return Promise.reject(new APIError(400, message));
            }
            const hasPassword = considerPasswordSpecified(passwordReceived);
            if (hasPassword) {
                return checkIfPasswordMatches(filename, passwordReceived);
            }
            return Promise.resolve();
        });
}

module.exports = {
    getAccessToDirectory,
    getAccessToDirectoryForFile,
    getAccessToFile,
};
